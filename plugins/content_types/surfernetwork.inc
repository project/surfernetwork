<?php
/**
 * @file
 * 
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Surfer Network block'),
  'description' => t('Output the pane for the Surfer Network streaming platform.'),
  'category' => t('Media'),
);

/**
 * An edit form for the pane's settings.
 *
 * Not enabled as there are no settings needed for this pane.
 */
function surfernetwork_surfernetwork_content_type_edit_form($form, &$form_state) {
  return $form;
}

/**
 * Render the pane.
 */
function surfernetwork_surfernetwork_content_type_render($subtype, $conf, $args, $contexts) {
  // Defer to the shared function.
  return _surfernetwork_output();
}
