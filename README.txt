SurferNetwork
-------------
This module allows you to output a link to play content from the SurferNetwork
streaming service [1].


Credits / Contact
------------------------------------------------------------------------------
Currently maintained by Damien McKenna [2]. Development is sponsored by
The Batavian [3].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  http://drupal.org/project/issues/surfernetwork


References
------------------------------------------------------------------------------
1: http://surfernetwork.com/
2: http://drupal.org/user/108450
3: http://www.thebatavian.com/
