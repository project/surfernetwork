<?php
/**
 * @file
 * Settings form.
 */

/**
 * FormAPI callback to load the 'surfernetwork_settings_form' form.
 */
function surfernetwork_settings_form() {
  $form = array();

  $form['surfernetwork_script_url'] = array(
    '#title' => t('Script URL'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('surfernetwork_script_url', 'http://nick8.surfernetwork.com/Media/player/scripts/launch.js'),
  );

  $form['surfernetwork_link_text'] = array(
    '#title' => t('Link text'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('surfernetwork_link_text', 'Listen Live'),
    '#description' => t('Will be used as the text for the link. To insert an image use: <code>&lt;img src="/path/to/image.png" alt="The Surfer Network" /&gt;</code><br />Only the following HTML tags are allowed: img, p, br, span'),
  );

  $form['surfernetwork_call_ltr'] = array(
    '#title' => t('"Call Later" code'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('surfernetwork_call_ltr'),
  );

  $form['surfernetwork_grp_tuner'] = array(
    '#title' => t('Group tuner (optional)'),
    '#type' => 'textfield',
    '#default_value' => variable_get('surfernetwork_grp_tuner'),
  );

  $form['surfernetwork_file'] = array(
    '#title' => t('File (optional)'),
    '#type' => 'textfield',
    '#default_value' => variable_get('surfernetwork_file'),
  );

  $form['surfernetwork_title'] = array(
    '#title' => t('Title (optional)'),
    '#type' => 'textfield',
    '#default_value' => variable_get('surfernetwork_title'),
  );

  return system_settings_form($form);
}
